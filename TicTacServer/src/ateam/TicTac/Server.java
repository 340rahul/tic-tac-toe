package ATeam.TicTac;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import ATeam.Packet.MessagePck;
import ATeam.mvc.ServerBroadcast;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : Server part in Server client pattern
Name of ADT: Server
Sets
* 	Server: set of functions that manages the server
*       Int: Primitive integer type(� -1, 0, 1, 2 �)
*       Thread  : Generic type that manages the threads
*       Socket   : Generic type that manages the socekt
*       ServerSocket: Generic type that manages serverSocket
*       boolean : Primitive boolean type( true, false, 1, 0)
*       ClientThread[] : Generic ClientThread type that is an array


Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class Server implements Runnable, ServerBroadcast<MessagePck, ClientThread>{
	
	private ClientThread [] clients;
	boolean xTaken = false;
	private ServerSocket serverSock;
    private Socket sock;
    private Thread thread = null;
    private int port = 5000;
    private int Clientcount = 0;
    
    /********************************************************************
    Function: Server
    *********************************************************************
    DESCRIPTION :
    �	Constructor
    [1] Create -> Server sv
    - Precondition	: 
    - Postcondition: sv is valid && Display strings && initialize the clients, serverSock
    *                and thread.
    ********************************************************************/
    public Server()
    {
        
    	System.out.println("Welcome!");
    	System.out.println("---------------------------------");
    	clients = new ClientThread[2]; 
        
        try
        {
            serverSock = new ServerSocket(port);
          
            System.out.println("Server started!"); 
            thread = new Thread(this);
            thread.start(); 
        }
        catch(IOException e)
        {
            System.out.println("Unable to start server: " + e);
        }
    }

    /********************************************************************
    Function: run
    *********************************************************************
    DESCRIPTION :
    �	Mutator
    [1] run(Server sv) -> Server sv'
    - Precondition	: true
    - Postcondition: sv' = sv where server connected to the client
    ********************************************************************/
	public void run() {
	
		while(thread!=null)
        {
            try
            {
               sock = serverSock.accept();

               if(Clientcount < clients.length)
               {  
                    clients[Clientcount] = new ClientThread(this, sock);
                    clients[Clientcount].start();
                                        
                    System.out.println("Connected to client, " + Clientcount + ".\n");
                                       
                    Clientcount++;
              }
               
              else
              {
            	  System.out.println("Client Limit Reached.\n");
              }
            }

            catch(IOException e)
            {
            	System.out.println("Cannot connect: " + e + "\n");
             }
        }
		
	}
	
	/********************************************************************
	Function: getClient
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getClient(Server sv, int clientNo) -> int i
	- Precondition	: true
	- Postcondition: returns the client number that matches the client ID
	********************************************************************/
	public int getClient(int clientNo)
    {
        for(int i = 0; i < Clientcount; i++)
        {
            if(clients[i].getID() == clientNo)
                return i;
        }

        return -1;
    }
	
	/********************************************************************
	Function: filter
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] filter(Server sv, MessagePck msg) -> Server sv'
	- Precondition	: true
	- Postcondition: sv' = sv where msg is broadcasted according to the type
	********************************************************************/
	public void filter(MessagePck msg){
		
		if(msg.getType().equals("REQ") && xTaken == false){
			
			MessagePck echo = new MessagePck("ACK", "server", msg.getSender(), msg.getData());
			echo.setToken('X');
			broadcast(echo);
			xTaken = true;
		}
		
		else if(msg.getType().equals("REQ") && xTaken == true){
			
			MessagePck deny = new MessagePck("DNY", "server", msg.getSender(), "O");
			broadcast(deny);
			
			
			System.out.println("O assigned to " + msg.getSender());
		}
		
		else if(msg.getType().equals("MOV")){
			
			broadcast(msg);
			
		}
		
		else{
			System.out.println("else");
			broadcast(msg);
		}
		
	}
	
	/********************************************************************
	Function: getThread
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getThread(Server sv, String str) -> ClientThread clients
	- Precondition	: true
	- Postcondition: returns the client that matches the string str
	********************************************************************/
	ClientThread getThread(String str)
    {
        for(int i = 0; i < Clientcount; i++)
        {
            if(clients[i].name.equals(str))
            {
                return clients[i];
            }
        }
        return null;
    }
	
	/********************************************************************
	Function: broadcast
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] broadcast(Server sv, MessagePck msg) -> Server sv'
	- Precondition	: true
	- Postcondition: sv' = sv where the msg is sent to the clients
	********************************************************************/
	public void broadcast(MessagePck msg)
    {
        for(int i = 0; i < Clientcount; i++)
        {
            clients[i].send(msg);
        }
    }


	@Override
	/********************************************************************
	Function: removeClient
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] removeClient(Server sv, ClientThread client) -> Server sv'
	- Precondition	: true
	- Postcondition: sv' = sv where client is closed
	********************************************************************/
	public void removeClient(ClientThread client) {
		
		client.close();
		
	}


	@Override
	/********************************************************************
	Function: endSession
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] endSession(Server sv) -> Server sv'
	- Precondition	: true
	- Postcondition: sv' = sv where the connection with clients is closed
	********************************************************************/
	public void endSession() {
		
		for(int i = 0; i < Clientcount; i++)
        {
            clients[i].close();
        }
		
	}
}
