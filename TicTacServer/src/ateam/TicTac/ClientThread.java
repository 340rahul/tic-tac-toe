package ATeam.TicTac;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import ATeam.Packet.MessagePck;
import ATeam.mvc.ThreadedClient;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : THis is MessagePack class/ Implements Serializable
Name of ADT: ClientThread
Sets
*       ClientThread: sets of functions that manages client threads
*       int     : Primitive integer type(..., 1, 2, 3, ...)
*       String  : Generic type that stores string type variables( ... "", "string", "apple", ...)
*       Socket   : Generic type that manages the socekt
*       Server   : Generic type that manages the server
*       ObjectInputStream   : Generic type that manages the input stream
*       ObjectOutputStream  : Generic type that manages the output stream
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class ClientThread extends Thread implements ThreadedClient<MessagePck>{

	public Socket sock;
    public Server server;
    public ObjectInputStream iStream;
    public ObjectOutputStream oStream;
    
    public int Id = -1;
    public String name;
    
    /********************************************************************
    Function: ClientThread
    *********************************************************************
    DESCRIPTION :
    �	Constructor
    [1] Create -> ClientThread ct
    - Precondition	: 
    - Postcondition: ct is valid && the variables in ct are initialized
    ********************************************************************/
    public ClientThread(Server srv, Socket clientSock)
    {
       
       try
       {
            oStream = new ObjectOutputStream(clientSock.getOutputStream());
            oStream.flush();
            iStream = new ObjectInputStream(clientSock.getInputStream());
       }
       catch(IOException e)
       {
           e.printStackTrace();
       }
        
       server = srv;
       Id = clientSock.getPort();
    
      
    }
    /********************************************************************
    Function: send
    *********************************************************************
    DESCRIPTION :
    �	Mutator
    [1] send(ClientThread ct, MessagePck message) -> ClientThread ct'
    - Precondition	: true
    - Postcondition: ct' = ct where oStream is flushed and message is written in the object
    ********************************************************************/
    public void send(MessagePck message)
    {
        try
        {
            oStream.flush();
            oStream.writeObject( message );
        }
        
        catch(IOException e)
        {
            e.printStackTrace();
        }
        
    }
    /********************************************************************
    Function: getID
    *********************************************************************
    DESCRIPTION :
    �	Accessor
    [1] getLocation(ClientThread ct) -> int Id
    - Precondition	: true
    - Postcondition: returns the int Id
    ********************************************************************/
    public int getID()
    {
        return this.Id;
    }
    /********************************************************************
    Function: close
    *********************************************************************
    DESCRIPTION :
    �	Mutator
    [1] close(ClientThread ct) -> ClientThread ct'
    - Precondition	: true
    - Postcondition: ct' = ct where sock is closed if sock is null,
    *               istream is closed if istream is null and
    *               ostream is closed if ostream is null/
    *               exception is thrown if error occured
    ********************************************************************/
    public void close()
    {  
    	try{
		 	if (sock != null)
		     {
		         sock.close();
		     }
		     
		     if (iStream != null)
		     {
		         iStream.close();
		     }
		     
		     if (oStream != null)
		     {
		         oStream.close();
		     }   
    	}catch(IOException e){
    		e.printStackTrace();
    	}
    }
    /********************************************************************
    Function: run
    *********************************************************************
    DESCRIPTION :
    �	Mutator
    [1] run(ClientThread ct) -> ClientThread ct'
    - Precondition	: true
    - Postcondition: ct' = ct where the message is set by the istream's object
    *                       and the exception is thrown if a class nof found
    ********************************************************************/
    public void run()
    {
        while(true)
        {
            try
            {
                    
                MessagePck msg = (MessagePck)iStream.readObject();
                server.filter(msg);
            }

            catch(IOException | ClassNotFoundException e)
            {
                e.printStackTrace();
                
            }
        }
    }
}
