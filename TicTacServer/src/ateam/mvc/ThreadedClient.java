package ATeam.mvc;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This is a Subject class for the Observer pattern
Name of ADT: ThreadedClient
Sets
*       ThreadedClient: Generic type that deals with array lists
*       M       : Generic type for the message
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public interface ThreadedClient <M> {
	
	public void send(M message);
	public void close();
	
}
