package ATeam.mvc;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This is a Subject class for the Observer pattern
Name of ADT: ServerBroadcast
Sets
*       ServerBroadcast: Generic type that deals with broadcasting the server
*       M       : Generic type for the message
*       C       : Generic type for the client
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public interface ServerBroadcast <M, C> {

	public void broadcast(M message);
	public void removeClient(C client);
	public void endSession();
	
}
