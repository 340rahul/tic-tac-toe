package ATeam.Packet;

import java.awt.Point;
import java.io.Serializable;

public class MessagePck implements Serializable{

	private String type, sender, receiver, data;
	private Point location = null;
	private char token = ' ';
	
	public MessagePck(String type, String sender, String receiver, String data){
		
		this.type = type;
		this.sender = sender;
		this.receiver = receiver;
		this.data = data;
		
	}
	
	public void setLocation(Point p){
		this.location = p;
	}
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	} 
	
	public char getToken() {
		return token;
	}
	public Point getLocation(){
		return this.location;
	}
	public void setToken(char token) {
		this.token = token;
	}
}
