package ATeam.mvc;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : View part in Model View Controller pattern
*               Implements viewable class(interface for view class),
*                   MouseListener class(manage mouse events),
*                   and Observer class
Name of ADT: Viewable
Sets
* 	Viewable: set of functions that manages the view
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public interface Viewable {

	public void initUI();
	
}
