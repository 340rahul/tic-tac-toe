package ATeam.mvc;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This is interface for the clientSide for client server pattern
Name of ADT: ClientSide
Sets
* 	ClientSide: set of functions that manages the client server
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public interface ClientSide <M, V>{
	
	public void connect();
	public void disconnect();
	public void send(M message);
	public void attachView(V view);
}
