package ATeam.mvc;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This is a Subject class for the Observer pattern
Name of ADT: Subject
Sets
*       Subject: Generic type that deals with array lists
*       Observer: Generic type that contains sets of functions that manages the observer
*       S       : Generic type for the status
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public interface Subject <S> {

	public void register(Observer<?> o);
	public void unregister(Observer<?> o);
	public void notifyObservers(S status);
	public void notifyObservers(Exception e);
	
}
