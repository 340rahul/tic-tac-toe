package ATeam.TicTac.Client;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION :
*   TicTacToegame with Model View Controller with Observer pattern
*   and Client server pattern with MVC and Observer Pattern.
********************************************************************/
public class Game {


	public static void main(String[] args) {
		
		Board board = new Board();
		Client c = new Client(board);
		View window1 = new View(c, board);
		window1.setVisible(true);
	}

}
