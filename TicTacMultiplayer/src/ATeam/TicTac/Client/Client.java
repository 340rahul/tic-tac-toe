package ATeam.TicTac.Client;

import java.net.*;
import java.awt.Point;
import java.io.*;
import ATeam.Packet.MessagePck;
import ATeam.mvc.ClientSide;
import ATeam.mvc.Observer;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : THis is MessagePack class/ Implements Serializable
Name of ADT: Client
Sets
*       Status  : Generic type that contains the set of functions that manages the status
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
*       String  : Generic type that stores string type variables( ... "", "string", "apple", ...)
*       Point    : Generic type that stores the point
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class Client implements ClientSide<MessagePck, View>, Runnable, Observer<MessagePck>{
	
	private String ip, clientName;
	private int port = 5000;
	private Socket sock;
	private ObjectOutputStream oStream;
	private ObjectInputStream iStream;
	private boolean isConnected = false;
	private View view;
	private Thread clientThread;
	private Board board;
	
	/********************************************************************
	Function: Client
	*********************************************************************
	DESCRIPTION :
	�	Constructor
	[1] Create -> Client c
	- Precondition      : 
	- Postcondition     : b is valid && board is initialized with board
	*           && clientThread is initialized with the new thread
	********************************************************************/
	public Client(Board board){
		
		this.board = board;
		clientThread = new Thread(this);
		
	}
	
	/********************************************************************
	Function: attachView
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] attachView(Client c, View view) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c where the variable view is set as the view
	*               ip is set as the view's ip, and clientName is set as the view's username
	********************************************************************/
	public void attachView(View view){
		
		this.view = view;
		ip = view.getIp();
		clientName = view.getUsername();
		
	}
	
	/********************************************************************
	Function: run
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] run(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c where exception is thrown if no class found
	********************************************************************/
	public void run() {
		
		try{
			while(isConnected){
				MessagePck msg = (MessagePck)iStream.readObject();
				System.out.println("Running");
				filter(msg);
			}
		}catch(IOException | ClassNotFoundException e){
			view.update(e);
		}
		
	}

	/********************************************************************
	Function: connect
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] connect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void connect() {
		
		try{
			sock = new Socket(ip, port);
			oStream = new ObjectOutputStream(sock.getOutputStream());
	        oStream.flush();
	        iStream = new ObjectInputStream(sock.getInputStream());
	        isConnected = true;
	        clientThread.start();
		}catch(IOException e){
			view.update(e);
		}
	}
	
	/********************************************************************
	Function: requestToken
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] requestToken(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c where c's req is initialized with the messagepck
	********************************************************************/
	public void requestToken(){
		
		MessagePck req = new MessagePck("REQ", clientName, "ip","X");
		req.setToken('X');
		send(req);
	}
	/********************************************************************
	Function: disconnect
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = sock, oStream and iStream are closed
	*               isConnected is set false and cient thread is stopped.
	********************************************************************/
	public void disconnect() {
		
		try {
			sock.close();
			oStream.close();
			iStream.close();
			isConnected = false;
			MessagePck m = new MessagePck(null, "Client", "Observer", "Disconnected from server!");
			view.update(m);
			
		} catch (IOException e) {
			
			view.update(e);
		}
		
	}
	
	/********************************************************************
	Function: playermove
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void playermove(Point point, char token){
		
		board.movePlayer(point, token);
		
		MessagePck move = new MessagePck("MOV", clientName, "server", null);
		move.setLocation(point);
		move.setToken(token);
		send(move);
		
	}
	
	/********************************************************************
	Function: opponentMove
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void opponentMove(Point point, char token){
		
		board.movePlayer(point, token);
		
	}
	
	/********************************************************************
	Function: cleanBoard
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void cleanBoard(){
		
		board.clean();
		MessagePck clean = new MessagePck("CLN", clientName, "server", null);
		send(clean);
		
	}
	
	/********************************************************************
	Function: filter
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void filter(MessagePck msg) {
		
		if(msg.getType().equals("ACK") && this.clientName.equals(msg.getReceiver())){
			
			Character[] t = {'X', 'O'};
			board.setToken(t);
			view.setWaiting(false);
			view.setClr("Orange");
		}
		
		else if(msg.getType().equals("DNY") && this.clientName.equals(msg.getReceiver())){
			
			Character[] t = {'O', 'X'};
			board.setToken(t);
			view.setWaiting(false);
			view.setClr("Red");
		}
		else if(msg.getType().equals("DNY") && !this.clientName.equals(msg.getReceiver())){
			
			Character[] t = {'X', 'O'};
			board.setToken(t);
			view.setWaiting(false);
			view.setClr("Orange");
			view.setTopText("Player started as " + "Orange");
			
		}
		
		else if(msg.getType().equals("MOV") && msg.getToken() != board.getToken(0)){
			
			System.out.println("Other msg received");
			opponentMove(msg.getLocation(), msg.getToken());
			
		}
		
	}
	
	/********************************************************************
	Function: send
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void send(MessagePck msg) {
		
		if(isConnected == true)
        {
            try
            {
               oStream.writeObject(msg);
               oStream.flush();
            }
            catch(IOException e)
            {
            	view.update(e);
               
            }
        }
        
        else
        {
        	MessagePck m = new MessagePck(null, "Client", "Observer", "Not connected to server!");
        	view.update(m);
        }
    }

	/********************************************************************
	Function: update
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void update(MessagePck status) {
		
	
	}

	/********************************************************************
	Function: update
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] disconnect(Client c) -> Client c'
	- Precondition	: true
	- Postcondition: c' = c with initilized sock, oStream and iStream &&
	*               isConnected is set true and cient thread is started
	********************************************************************/
	public void update(Exception e) {
		
	}
}
