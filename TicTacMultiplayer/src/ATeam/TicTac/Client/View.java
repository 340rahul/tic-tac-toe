package ATeam.TicTac.Client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ATeam.Packet.MessagePck;
import ATeam.mvc.Observer;
import ATeam.mvc.Viewable;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : View part in Model View Controller pattern/ 
*             Extends JFrame for UI, 
*             Implements viewable class(interface for view class),
*             MouseListener class(manage mouse events),
*             and Observer class
Name of ADT: View
Sets
* 	View: set of functions that manages the view
*       Int: Primitive integer type(� -1, 0, 1, 2 �)
*       ArrayList<JLabel>: Generic type that stores JLabel type elements
*       JPanel  : Generic type that controls the Panel
*       JLabel  : Generic type that controls the label
*       Board   : Generic type that contains sets of functions that manages the board
*       Controller: Generic type that contains sets of functions that manages controller
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
*       AI      : Generic type that manages the AI

Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class View extends JFrame implements Viewable, MouseListener, Observer<MessagePck>{

	private ArrayList <JLabel> boxes = new ArrayList <JLabel> ();
	private JPanel panel, messagePanel;
	private JLabel topText;
	private Client client;	
	private String ip;
	private String username;
	private Board board;
	private boolean waiting = true;
	private String clr;
	
	/********************************************************************
	Function: View
	*********************************************************************
	DESCRIPTION :
	�	Constructor
	[1] Create -> View v
	- Precondition      : true
	- Postcondition     : v is valid &&
	*Variables such as board, ctr, player1, player2, currentPlayer bot1
	*are initialized.
	********************************************************************/
	public View(Client client, Board board){
		
		this.client = client;
		this.board = board;
		this.board.register(client);
		this.board.register(this);
		askIp();
		askUsername();
		client.attachView(this);
		client.connect();
		client.requestToken();
		initUI();
		
	}
	
	/********************************************************************
	Function: askIp
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] askIp(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where creates a J Option Pane to ask user IP address
	********************************************************************/
	private void askIp(){
		
		this.setIp(JOptionPane.showInputDialog(this, "Enter the IP to connect to ", "IP address required", 
									JOptionPane.INFORMATION_MESSAGE));
				
	}
	
	/********************************************************************
	Function: askUsername
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] askUsername(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the created JOption pane to ask user the username
	********************************************************************/
	private void askUsername(){
		
		this.setUsername(JOptionPane.showInputDialog(this, "Enter your username ", "Username required", 
				JOptionPane.INFORMATION_MESSAGE));
						
	}
	
	/********************************************************************
	Function: initUI
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setCurrentPlayer(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the frame, messagePanel, Panel and board is set
	********************************************************************/

	public void initUI() {
		
		setupFrame();
        setupMessagePanel();
        setupPanel();
        setupBoard();

        this.getContentPane().add(panel);
        panel.addMouseListener(this);
        
		
	}

	/********************************************************************
	Function: setupFrame
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setupFrame(View v) -> View v'
	- Precondition	: true
	- Postcondition: v' = v with the changed size of the frame to 400, 400,
	*                   the title of "TicTacToe",
	*                   the default close operation, EXIT_ON_CLOSE,
	*                   the location relative to null,
	*                   and layout as borderLayout.                 
	********************************************************************/
	private void setupFrame(){
	
		setSize(400, 400);
        setTitle("TicTacToe");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
		
	}
	
	/********************************************************************
	Function: setupMessagePanel
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setupMessagePanel(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the new JPanel,
	*                   new JLabel with the message "Player started as X",
	*                   the topText added to the mesagePanel 
	*                   and messagePanel added to the north of BorderLayout of contentPane.
	********************************************************************/
	private void setupMessagePanel() {
		
	    messagePanel = new JPanel();
	    topText = new JLabel("Player started as " + clr);
	    messagePanel.add(topText);
	
	    this.getContentPane().add(messagePanel, BorderLayout.NORTH);

	}
	
	public void setTopText(String text){
		
		topText.setText(text);
		
	}
	
	/********************************************************************
	Function: setupBoard
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setupBoard(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the text is all initialized to null,
	*                   each dimension is set as 400/3 by 400/3,
	*                   Opoaque was set as true to allow coloring tothe background,
	*                   background is set as lightgray
	*                   horizontal alignment is set as center
	*                   and border is set as the thick of 2 and color CYAN
	********************************************************************/
	private void setupBoard() {
		
        for (int i = 0; i < 9; i++) {
        	
            JLabel symbol = new JLabel();
            symbol.setText("");
            symbol.setSize(new Dimension(400 / 3, 400 / 3));
            symbol.setOpaque(true);
            symbol.setBackground(Color.lightGray);
            symbol.setHorizontalAlignment(JLabel.CENTER);
            symbol.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.CYAN));
         

            boxes.add(symbol);
            panel.add(symbol);
            
        }
    }
	
	
/********************************************************************
Function: setupPanel
*********************************************************************
DESCRIPTION :
�	Mutator
[1] setupPanel(View v) -> View v'
- Precondition	: true
- Postcondition: v'= v where panel is initialized with new JPanel type
*               and layout is set as 3, 3, -1, -1
********************************************************************/
	private void setupPanel() {
		
        panel = new JPanel();

        panel.setLayout(new GridLayout(3, 3, -1, -1));
        
    }
	
	/********************************************************************
	Function: mouseClicked
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mouseClicked(View v) -> View v'
	- Precondition	: true
	- Postcondition : v'= v with the event sent when the mouse is clicked            
	********************************************************************/
	public void mouseClicked(MouseEvent arg0) {
		
	}

		
	/********************************************************************
	Function: mouseEntered
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mouseEntered(View v) -> View v'
	- Precondition      : true
	- Postcondition     : v'= v where the event is thrown when the mouse is entered            
	********************************************************************/
	public void mouseEntered(MouseEvent arg0) {
		
	}

		
		
	/********************************************************************
	Function: mouseExited
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mouseExited(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the event is sent when the mouse is exited
	********************************************************************/
	public void mouseExited(MouseEvent arg0) {
		
	}

		
		
	/********************************************************************
	Function: mousePressed
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mousePressed(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the event is thrown when the mouse is pressed
	********************************************************************/
	public void mousePressed(MouseEvent arg0) {
		
	}
	
	/********************************************************************
	Function: mouseReleased
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setCurrentPlayer(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where event is thrown when the mouse is released.
	*When it is released, it passes the information to the controller with the 
	*playermove method
	********************************************************************/
	public void mouseReleased(MouseEvent arg0) {
		
		if(!waiting){
			
			for (int i = 0; i < boxes.size(); i++) {
	
	            //if one of the JLabels bounds contains the point where the mouse click 
	            //was released, passes the information to the client with the  
	            //playermove method
	            if (boxes.get(i).getBounds().contains(arg0.getPoint())) {
	            	
	            	client.playermove(new Point(i / 3, i % 3), board.getToken(0));
	            }
				
			}
		}
		
	}
	
	/********************************************************************
	Function: getIp
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getIp(View v) -> String ip
	- Precondition	: true
	- Postcondition: Returns the String type IP
	********************************************************************/
	public String getIp() {
		return ip;
	}
	
	/********************************************************************
	Function: setIp
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setIp(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where ip is set as string ip
	********************************************************************/
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	/********************************************************************
	Function: getUsername
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getUsername(View v) -> String username
	- Precondition	: true
	- Postcondition: Returns the String type username
	********************************************************************/
	public String getUsername() {
		return username;
	}
	
	/********************************************************************
	Function: setUsername
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setUsername(View v, String username) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where username is set as the stringn type username
	********************************************************************/
	public void setUsername(String username) {
		this.username = username;
	}

	/********************************************************************
	Function: update
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] update(View v) -> View v'
	- Precondition      : true
	- Postcondition     : v'= v where the status of type is move, player move and switch the player
	*                   if the status type is win, set the toptext as showing who won,
	*                   if the status type is tie, set the totext as showing the game is a tie
	*                   and if the status type is cln,  than clean everything and start new AI.
	********************************************************************/
	public void update(MessagePck msg) {
		
		if(msg.getType() == "TIE"){
			JOptionPane.showMessageDialog(this, "The game is a Tie :-|", "Game Ended", JOptionPane.INFORMATION_MESSAGE);
			waiting = true;
		}
		
		
		if(msg.getType() == "MOV"){
			
			if(msg.getToken() == board.getToken(0)){
				
				playerMove(msg.getLocation().y + 3 * msg.getLocation().x, msg.getToken());
				waiting = true;
			}
			else{
				
					playerMove(msg.getLocation().y + 3 * msg.getLocation().x, msg.getToken());
					waiting = false;
			}
			
		}
		
		if(msg.getType() == "WIN"){
			
			if(msg.getData() == "YOU"){
				JOptionPane.showMessageDialog(this, "You Won!", "Game Ended", JOptionPane.INFORMATION_MESSAGE);
			}
			else{
				JOptionPane.showMessageDialog(this, "You Lost :-( ", "Game Ended", JOptionPane.INFORMATION_MESSAGE);

			}
							
			waiting = true;
		}
		
	}
	
	/********************************************************************
	Function: update
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] update(View v) -> View v'
	- Precondition      : true
	- Postcondition     : v'= v where the excpetion is thrown
	********************************************************************/
	public void update(Exception e) {
		
		JOptionPane.showMessageDialog(this, e, "Exception!", JOptionPane.ERROR_MESSAGE);
		
	}

	/********************************************************************
	Function: isWaiting
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] isWaiting(View v) ->boolean  waiting
	- Precondition	: true
	- Postcondition: Returns the boolean type waiting
	********************************************************************/
	public boolean isWaiting() {
		return waiting;
	}
	
	/********************************************************************
	Function: setWaiting
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setWaiting(View v, boolean waiting) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the state of waiting is changed by the boolean waiting
	********************************************************************/
	public void setWaiting(boolean waiting) {
		this.waiting = waiting;
	}
	
	/********************************************************************
	Function: playerMove
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] playerMove(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the modified variable of currentPlayer with the player.             
	********************************************************************/
	private void playerMove(int position, char token){
		
		drawSymbol(position, token);
		
	}
	
	/********************************************************************
	Function: drawSymbol
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] drawSymbol(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the background set as red for player O
	*                   and orange for player X
	********************************************************************/
	private void drawSymbol(int index, char symbol) {
		
        if (symbol == 'O') {
        	boxes.get(index).setBackground(Color.red);
            
        } 
        
        else {
        	boxes.get(index).setBackground(Color.ORANGE);
            
        }
    }
	
	/********************************************************************
	Function: getClr
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getClr(View v) ->String  clr
	- Precondition	: true
	- Postcondition: Returns the String type clr
	********************************************************************/
	public String getClr() {
		return clr;
	}
	
	/********************************************************************
	Function: setClr
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setClr(View v, String clr) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where clr is set as the stringn type clr
	********************************************************************/
	public void setClr(String clr) {
		this.clr = clr;
	}
	
	/********************************************************************
	Function: clean
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] clean(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where every boxes of label is cleaned with null text
	*               and set every color as light gray(default color)
	********************************************************************/
	private void clean() {
		
        for (JLabel label : boxes) {
            label.setText("");
        }
        
        for(int index = 0; index < boxes.size(); index++){
        	
        	boxes.get(index).setBackground(Color.lightGray);
        	
        }

        panel.repaint();
     }
}
