package ATeam.Packet;

import java.awt.Point;
import java.io.Serializable;


/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : THis is MessagePack class/ Implements Serializable
Name of ADT: View
Sets
*       Status  : Generic type that contains the set of functions that manages the status
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
*       String  : Generic type that stores string type variables( ... "", "string", "apple", ...)
*       Point    : Generic type that stores the point
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class MessagePck implements Serializable{

	private String type, sender, receiver, data;
	private Point location = null;
	private char token = ' ';
	
	/********************************************************************
	Function: MessagePck
	*********************************************************************
	DESCRIPTION :
	�	Constructor
	[1] Create -> MessagePck mp
	- Precondition	: 
	- Postcondition: mp is valid && initialize type, sender receiver and data variables
	********************************************************************/
	public MessagePck(String type, String sender, String receiver, String data){
		
		this.type = type;
		this.sender = sender;
		this.receiver = receiver;
		this.data = data;
			
	}
	
	/********************************************************************
	Function: getLocation
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getLocation(MessagePck mp) -> Point location
	- Precondition	: true
	- Postcondition: returns the Point type location
	********************************************************************/
	public Point getLocation(){
		
		return this.location;
		
	}
	
	/********************************************************************
	Function: setLocation
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setLocation(MessagePck mp, Point p) -> MessagePck mp
	- Precondition	: true
	- Postcondition: mp' = mp where the location is set as Point p
	********************************************************************/
	public void setLocation(Point p){
		this.location = p;
	}
	
	/********************************************************************
	Function: getSender
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getSender(MessagePck mp) -> String sender
	- Precondition	: true
	- Postcondition: returns String type sender
	********************************************************************/
	public String getSender() {
		return sender;
	}
	
	/********************************************************************
	Function: setSender
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setSender(MessagePck mp) -> MessagePck mp'
	- Precondition	: true
	- Postcondition: mp' = mp where sender is set as String typed sender
	********************************************************************/
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	/********************************************************************
	Function: getData
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getData(MessagePck mp) -> String data
	- Precondition	: true
	- Postcondition: returns String type data
	********************************************************************/
	public String getData() {
		return data;
	}
	
	/********************************************************************
	Function: setData
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setData(MessagePck mp, String data) -> MessagePck mp'
	- Precondition	: true
	- Postcondition: mp' = mp where data is set as String type data
	********************************************************************/
	public void setData(String data) {
		this.data = data;
	}
	
	/********************************************************************
	Function: getReceiver
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getLocation(MessagePck mp) -> String reciever
	- Precondition	: true
	- Postcondition: returns the String type reciever
	********************************************************************/
	public String getReceiver() {
		return receiver;
	}
	
	/********************************************************************
	Function: setReceiver
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setReceiver(MessagePck mp, String receiver) -> MessagePck mp'
	- Precondition	: true
	- Postcondition: mp' = mp where reciever is set as String type reciever
	********************************************************************/
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
	/********************************************************************
	Function: getType
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getType(MessagePck mp) -> String type
	- Precondition	: true
	- Postcondition: returns the String 'type'
	********************************************************************/
	public String getType() {
		return type;
	}

	/********************************************************************
	Function: setType
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setType(MessagePck mp, String type) -> MessagePck mp'
	- Precondition	: true
	- Postcondition: mp' = mp where type is set
	********************************************************************/
	public void setType(String type) {
		this.type = type;
	}
	
	/********************************************************************
	Function: getToken
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getToken(MessagePck mp) -> char token
	- Precondition	: true
	- Postcondition: returns character type token
	********************************************************************/
	public char getToken() {
		return token;
	}
	
	/********************************************************************
	Function: setToken
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setToken(MessagePck mp, char token) -> MessagePck mp'
	- Precondition	: true
	- Postcondition: mp' = mp where token is set as char token
	********************************************************************/
	public void setToken(char token) {
		this.token = token;
	} 
	
}
