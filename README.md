# Tic-tac-toe
Java based simple tic-tac-toe game designed using Swing.  
  
TicTacToe contains single player game client.  
TicTacToeMultiplayer is the multiplayer client.  
Use TicTacServer to create and start a game server.  