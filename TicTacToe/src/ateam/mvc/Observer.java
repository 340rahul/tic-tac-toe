package ATeam.mvc;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This is the observer interfaceclass
Name of ADT: Observer
Sets
*       Observer: Generic type that deals with observer
*       S       : Generic type for the status
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public interface Observer <S> {
	
	public void update(S status);

}
