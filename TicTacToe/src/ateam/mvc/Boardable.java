package ATeam.mvc;

import java.awt.Point;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This is the class Board that implements 
*               The class Subject and Boardable
*               This is model part in mvc pattern
Name of ADT: Board
Sets
*       ArrayList: Generic type that deals with array lists
*       Int: Primitive integer type(� -1, 0, 1, 2 �)
*       S       : Generic type for the status
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public interface Boardable <T> {
		
	public void setSize(int x, int y);
	public void movePlayer(Point location, T token);
	public void setToken(T [] token);
	public void setPlayers(int numberOfPlayers);
	public void clean();
	public T getWinner();
}
