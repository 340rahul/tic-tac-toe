package ATeam.game;

import java.awt.Point;
import java.util.ArrayList;

import ATeam.dat.Status;
import ATeam.mvc.Boardable;
import ATeam.mvc.Observer;
import ATeam.mvc.Subject;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This is the class Board that implements 
*               The class Subject and Boardable
*               This is model part in mvc pattern
Name of ADT: Board
Sets
*       ArrayList: Generic type that deals with array lists
*       Board   : Generic type that contains sets of functions that manages the board
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
*       char array: array of Primitive character type
*       Int: Primitive integer type(� -1, 0, 1, 2 �)
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class Board implements Subject<Status>, Boardable<Character>{
	
	private ArrayList <Observer> o;
	private char [][] box;
	private char [] tokens;
	private int numberOfPlayers = 0;
	private int xSize = 0, ySize = 0;
	
	/********************************************************************
	Function: Board
	*********************************************************************
	DESCRIPTION :
	�	Constructor
	[1] Create -> Board b
	- Precondition      : 
	- Postcondition     : b is valid && board is cleaned, set 0's token as 'x'
	*                   set '1's token as 'o', set the box as a new character array
	*                   of size of xSize*ySize, set tokens as a new character array.
	********************************************************************/
	public Board(){
		
		o = new ArrayList<Observer>();
		setSize(3, 3);
		setPlayers(2);
		box = new char[xSize][ySize];
		tokens = new char[numberOfPlayers];
		this.clean();
		
		//Player1 is assigned X by default:
		tokens [0] = 'X';
		tokens [1] = 'O';
	}

	/********************************************************************
	Function: register
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] register(Board b, Observer observer) -> Board b'
	- Precondition	: true
	- Postcondition: v' = v where the observer is added to this Observertype Arraylist
	********************************************************************/
	public void register(Observer observer) {
		
		this.o.add(observer);
		
	}
	
	/********************************************************************
	Function: unregister
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] unregister(View v) -> View v'
	- Precondition  : true
	- Postcondition : v' = v where the observer is removed from the the Observer type's arraylist               
	********************************************************************/
	public void unregister(Observer observer) {
		
		int index = o.indexOf(observer);
		o.remove(index);
		
	}
	
	/********************************************************************
	Function: notifyObservers
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] notifyObservers(Board b, Status status) -> Board b'
	- Precondition	: true
	- Postcondition: v' = v where the observer's status is updated       
	********************************************************************/
	public void notifyObservers(Status status) {
		
		for(Observer obs : o ){
			
			obs.update(status);
			
		}
		
	}
	
	/********************************************************************
	Function: getToken
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getToken(Board b, int index) -> tokens
	- Precondition	: true
	- Postcondition: returns the token of the index
	********************************************************************/
	public char getToken( int index ){
		
		return this.tokens[index];
		
	}
	
	/********************************************************************
	Function: setPlayers
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setPlayers(Board b) -> Board b'
	- Precondition	: true
	- Postcondition: b' = b where the number of player is set by the integer numberOfPlayer                
	********************************************************************/
	public void setPlayers(int numberOfPlayers){
	
		this.numberOfPlayers = numberOfPlayers;
		
	}
	
	/********************************************************************
	Function: clean
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] clean(Board b) -> Board b'
	- Precondition	: true
	- Postcondition: b' = b where all the values in box are set as 0 and
	*               notify the changed status to the observer
	********************************************************************/
    public void clean() {
    	
        for (int i = 0; i < 3; i++) {
        	
            for (int j = 0; j < 3; j++) {
            	
                box[i][j] = 0;
                
            }
            
        }
        
        this.notifyObservers(new Status("CLN", tokens[0], null));
        
    }

    /********************************************************************
    Function: setSize
    *********************************************************************
    DESCRIPTION :
    �	Mutator
    [1] setSize(Board b, int x, int y) -> Board b'
    - Precondition	: true
    - Postcondition: b' =b where xSize is set as x and ySize is set as y                
    ********************************************************************/
	public void setSize(int x, int y) {
		
		this.xSize = x;
		this.ySize = y;
		
	}
	
	/********************************************************************
	Function: getBoxAt
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getBoxAt(Board b, int x, int y) -> char token
	- Precondition	: true
	- Postcondition: returns the tokens from the box at the x and y coordinate
	*               if x and y is out of the board, returns '-'
	********************************************************************/
	public char getBoxAt(int x, int y){
		
		
		if( x < xSize && y < ySize && x >= 0 && y >= 0)
			return this.box[x][y];
		
		else
			return '-';		//Moved out of board!
		
	}

	/********************************************************************
	Function: movePlayer
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] movePlayer(Board b, Point location, Character token) -> Board b'
	- Precondition  : true
	- Postcondition : b' =b where player is moved to the location and place the token into it
	********************************************************************/
	public void movePlayer(Point location, Character token) {
		
		// if the board is empty at the selected location, a move is made
        if (box[location.x][location.y] == 0 && token == tokens[0]) {
            makeMove(tokens [0], location);
        }
        
        else if (box[location.x][location.y] == 0 && token == tokens[1]) {
            makeMove(tokens [1], location);
        }
        
        else{
        	//else notify observer about the failure to move. 
        	//BNE : BoxNotEmpty
        	Status s = new Status("BNE", tokens [0], location);
        	notifyObservers(s);
        }
		
        //Post-move checks:
		
		if(getWinner() != null){
			//Move not possible. Game has ended:
			Status s = new Status("WIN", getWinner(), null);
			notifyObservers(s);
		}
		
		if(gameTie()){
			
			Status s = new Status("TIE", tokens [0], null);
		  	notifyObservers(s);
			
		}
        
	}

	
	/********************************************************************
	Function: makeMove
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] makeMove(Board b, char t, Point position) -> Board b'
	- Precondition	: true
	- Postcondition: b' = b where character t is inserted into the box 
	*               and the status is updated by the position
	*               and the status is notified to the observers
	********************************************************************/
	private void makeMove(char t, Point position) {
		
        box[position.x][position.y] = t;
        Status s = new Status("MOV", t, position);		//MOV : Move
        notifyObservers(s);
        
    }

	
	/********************************************************************
	Function: setToken
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setToken(Board b, Character[] token) -> Board b'
	- Precondition	: true
	- Postcondition: b' = b where tokens are set as the token                 
	********************************************************************/
	public void setToken(Character[] token) {
		
		this.tokens [0] = token[0];
		this.tokens [1] = token[1];
		
	}

	/********************************************************************
	Function: getXSize
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getXSize(Board b) -> int xSize
	- Precondition	: true
	- Postcondition: returns the 'xSize'               
	********************************************************************/
	public int getXSize(){
		return xSize;
	}
	
	/********************************************************************
	Function: getYSize
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getYSize(Board b) -> int ySize;
	- Precondition	: true
	- Postcondition: returns the 'ySize'           
	********************************************************************/
	public int getYSize(){
		return ySize;
	}

	/********************************************************************
	Function: getWinner
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getWinner(Board b) -> Character
	- Precondition  : true
	- Postcondition : returns the winner. if tied return null 
	********************************************************************/
	public Character getWinner() {
		
		if (box[0][0] != 0 && box[0][0] == box[1][1] && box[1][1] == box[2][2]) {
            return box[0][0];
        } 
		else if (box[0][2] != 0 && box[0][2] == box[1][1] && box[1][1] == box[2][0]) {
            return(box[0][2]);
        } 
		else {
            for (int i = 0; i < 3; i++) {
                if (box[i][0] != 0 && box[i][0] == box[i][1] && box[i][1] == box[i][2]) {
                    return(box[i][0]);
                    
                }
                if (box[0][i] != 0 && box[0][i] == box[1][i] && box[1][i] == box[2][i]) {
                    return(box[0][i]);
                    
                }
            }
        }
		
		return null;
	}
	
	/********************************************************************
	Function: gameTie
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] gameTie(Board b) -> boolean bool
	- Precondition	: true
	- Postcondition: returns flase if the contents of the box is 0
	********************************************************************/
	private boolean gameTie(){
		
		if(getWinner() != null){
			
			return false;
			
		}
		
		for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                if (box[x][y] == 0) {
                    return false;
                }
            }
        }
		return true;
	}

}
