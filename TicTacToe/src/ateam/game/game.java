package ATeam.game;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION :
*   TicTacToegame with Model View Controller with Observer pattern
*   and Client server with  MVC and Observer pattern.
********************************************************************/
public class game {

	public static void main(String[] args) {
		
		Board b = new Board();
		View window1 = new View(b);
		window1.setVisible(true);
	}

}
