package ATeam.game;

import java.awt.Point;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This class manages the AI response for the game TICTACTOE
Name of ADT: AI
Sets
*       AI      : set of functions that deals with AI
*       Point   : Generic type to store the move
*       Board   : Generic type that deals with the board
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
*       Int: Primitive integer type(� -1, 0, 1, 2 �)
*       boolean : Primitive boolean type
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class AI{

	private boolean firstMove = true;
	private Point bestMove = new Point();
	private Board board;
	private char token;
	private char enemyToken;
	
	/********************************************************************
	Function: AI
	*********************************************************************
	DESCRIPTION :
	�	Constructor
	[1] Create -> AI ai
	- Precondition	: 
	- Postcondition: ai is valid && board is set as b, token is set as t and enemyToken is set as et                
	********************************************************************/
	public AI(Board b, char t, char et){
		
		this.board = b;
		this.token = t;
		this.enemyToken = et;
			
	}
	
	/********************************************************************
	Function: thinkMove
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] thinkMove(AI ai) -> Point p
	- Precondition  : true
	- Postcondition : returns the bestMove by evaluating each possible move                 
	********************************************************************/
	public Point thinkMove(){
		
		if(firstMove){
			//If center is empty:
			if(board.getBoxAt(1, 1) == 0){
				bestMove.x = 1;
				bestMove.y = 1;
				firstMove = false;
				return bestMove;
			}
			else{
				//Move at top right box if center is not empty:
				bestMove.x = 0;
				bestMove.y = 0;
				firstMove = false;
				return bestMove;
			}
		}
		else{
			boolean moveFound = false;
			//Look for possible wins:
			for(int x = 0; x < board.getXSize(); x++){
				for(int y = 0; y < board.getYSize(); y++){
					//For every AI token found:
					if(board.getBoxAt(x, y) == token){
						//If token found at 1stCol:
						if(y == 0){
							//look for more immediate horizontal tokens:
							if(board.getBoxAt(x, y+1) == token
									&& board.getBoxAt(x, y+2) == 0){
								bestMove.x = x;
								bestMove.y = y+2;
								moveFound = true;
								break;
							}
							else if(board.getBoxAt(x, y+2) == token
									&& board.getBoxAt(x, y+1) == 0){
								bestMove.x = x;
								bestMove.y = y+1;
								moveFound = true;
								break;
							}
							//Corner Box. Look for diagonal chances too:
							else if(board.getBoxAt(1, 1) == token 
									&& board.getBoxAt(2, 2)==0){
								bestMove.x = 2;
								bestMove.y = 2;
								moveFound = true;
								break;
							}
							//If immediate horizontal tokens are not found, look for vertical:
							else if(x == 1){
								if(board.getBoxAt(x-1, y) == token
										&& board.getBoxAt(x+1, y) == 0){
									bestMove.x = x+1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x+1, y) == token 
										&& board.getBoxAt(x-1, y) == 0){
									bestMove.x = x-1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
							}
							else if(x == 2){
								if(board.getBoxAt(x-1, y) == token
										&& board.getBoxAt(x-2, y) == 0){
									bestMove.x = x-2;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x-2, y) == token 
										&& board.getBoxAt(x-1, y) == 0){
									bestMove.x = x-1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								//Corner Box. Look for diagonal chances too:
								else if(board.getBoxAt(1, 1) == token 
										&& board.getBoxAt(0, 2)==0){
									bestMove.x = 0;
									bestMove.y = 2;
									moveFound = true;
									break;
								}
							}
						}
						//If token found at 2ndCol:
						if(y == 1){
							//look for more immediate horizontal tokens:
							if(board.getBoxAt(x, y+1) == token
									&& board.getBoxAt(x, y-1) == 0){
								bestMove.x = x;
								bestMove.y = y-1;
								moveFound = true;
								break;
							}
							else if(board.getBoxAt(x, y-1) == token
									&& board.getBoxAt(x, y+1) == 0){
								bestMove.x = x;
								bestMove.y = y+1;
								moveFound = true;
								break;
							}
							//If immediate horizontal tokens are not found, look for vertical:
							else if(x == 1){
								if(board.getBoxAt(x-1, y) == token
										&& board.getBoxAt(x+1, y) == 0){
									bestMove.x = x+1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x+1, y) == token 
										&& board.getBoxAt(x-1, y) == 0){
									bestMove.x = x-1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
							}
							else if(x == 2){
								if(board.getBoxAt(x-1, y) == token
										&& board.getBoxAt(x-2, y) == 0){
									bestMove.x = x-2;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x-2, y) == token 
										&& board.getBoxAt(x-1, y) == 0){
									bestMove.x = x-1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
							}
						}
						//If token found at 3rdCol:
						if(y==2){
							//look for more immediate horizontal tokens:
							if(board.getBoxAt(x, y-1) == token
									&& board.getBoxAt(x, y-2) == 0){
								bestMove.x = x;
								bestMove.y = y-2;
								moveFound = true;
								break;
							}
							else if(board.getBoxAt(x, y-2) == token
									&& board.getBoxAt(x, y-1) == 0){
								bestMove.x = x;
								bestMove.y = y-1;
								moveFound = true;
								break;
							}
							//Corner Box. Look for diagonal chances too:
							else if(board.getBoxAt(1, 1) == token 
									&& board.getBoxAt(2, 0)==0){
								bestMove.x = 2;
								bestMove.y = 0;
								moveFound = true;
								break;
							}
							//If immediate horizontal tokens are not found, look for vertical:
							else if(x == 1){
								if(board.getBoxAt(x-1, y) == token
										&& board.getBoxAt(x+1, y) == 0){
									bestMove.x = x+1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x+1, y) == token 
										&& board.getBoxAt(x-1, y) == 0){
									bestMove.x = x-1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
							}
							else if(x == 2){
								if(board.getBoxAt(x-1, y) == token
										&& board.getBoxAt(x-2, y) == 0){
									bestMove.x = x-2;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x-2, y) == token 
										&& board.getBoxAt(x-1, y) == 0){
									bestMove.x = x-1;
									bestMove.y = y;
									moveFound = true;
									break;
								}
								//Corner Box. Look for diagonal chances too:
								else if(board.getBoxAt(1, 1) == token 
										&& board.getBoxAt(0, 0)==0){
									bestMove.x = 0;
									bestMove.y = 0;
									moveFound = true;
									break;
								}
							}
						}
					}
				}
			}
			if(!moveFound){
				//If moves not found there are no possible win scenario.
				//Defend:
				for(int x = 0; x < board.getXSize(); x++){
					for(int y = 0; y < board.getYSize(); y++){
						//For every AI token found:
						if(board.getBoxAt(x, y) == enemyToken){
							//If token found at 1stCol:
							if(y == 0){
								//look for more immediate horizontal tokens:
								if(board.getBoxAt(x, y+1) == enemyToken
										&& board.getBoxAt(x, y+2) == 0){
									bestMove.x = x;
									bestMove.y = y+2;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x, y+2) == enemyToken
										&& board.getBoxAt(x, y+1) == 0){
									bestMove.x = x;
									bestMove.y = y+1;
									moveFound = true;
									break;
								}
								//If immediate horizontal tokens are not found, look for vertical:
								else if(x == 1){
									if(board.getBoxAt(x-1, y) == enemyToken
											&& board.getBoxAt(x+1, y) == 0){
										bestMove.x = x+1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									else if(board.getBoxAt(x+1, y) == enemyToken 
											&& board.getBoxAt(x-1, y) == 0){
										bestMove.x = x-1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
								}
								else if(x == 2){
									if(board.getBoxAt(x-1, y) == enemyToken
											&& board.getBoxAt(x-2, y) == 0){
										bestMove.x = x-2;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									else if(board.getBoxAt(x-2, y) == enemyToken 
											&& board.getBoxAt(x-1, y) == 0){
										bestMove.x = x-1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									//Corner Box. Look for diagonal chances too:
									if(board.getBoxAt(1, 1) == enemyToken 
											&& board.getBoxAt(0, 2)==0){
										bestMove.x = 0;
										bestMove.y = 2;
										moveFound = true;
										break;
									}
								}
							}
							//If token found at 2ndCol:
							if(y == 1){
								//look for more immediate horizontal tokens:
								if(board.getBoxAt(x, y+1) == enemyToken
										&& board.getBoxAt(x, y-1) == 0){
									bestMove.x = x;
									bestMove.y = y-1;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x, y-1) == enemyToken
										&& board.getBoxAt(x, y+1) == 0){
									bestMove.x = x;
									bestMove.y = y+1;
									moveFound = true;
									break;
								}
								//If immediate horizontal tokens are not found, look for vertical:
								else if(x == 1){
									if(board.getBoxAt(x-1, y) == enemyToken
										&& board.getBoxAt(x+1, y) == 0){
										bestMove.x = x+1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									else if(board.getBoxAt(x+1, y) == enemyToken 
											&& board.getBoxAt(x-1, y) == 0){
										bestMove.x = x-1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
								}
								else if(x == 2){
									if(board.getBoxAt(x-1, y) == enemyToken
										&& board.getBoxAt(x-2, y) == 0){
										bestMove.x = x-2;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									else if(board.getBoxAt(x-2, y) == enemyToken 
											&& board.getBoxAt(x-1, y) == 0){
										bestMove.x = x-1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									//Corner Box. Look for diagonal chances too:
									else if(board.getBoxAt(1, 1) == enemyToken 
											&& board.getBoxAt(2, 0)==0){
										bestMove.x = 2;
										bestMove.y = 0;
										moveFound = true;
										break;
									}
								}
							}
							//If token found at 3rdCol:
							if(y==2){
								//look for more immediate horizontal tokens:
								if(board.getBoxAt(x, y-1) == enemyToken
										&& board.getBoxAt(x, y-2) == 0){
									bestMove.x = x;
									bestMove.y = y-2;
									moveFound = true;
									break;
								}
								else if(board.getBoxAt(x, y-2) == enemyToken
										&& board.getBoxAt(x, y-1) == 0){
									bestMove.x = x;
									bestMove.y = y-1;
									moveFound = true;
									break;
								}
								//If immediate horizontal tokens are not found, look for vertical:
								else if(x == 1){
									if(board.getBoxAt(x-1, y) == enemyToken
											&& board.getBoxAt(x+1, y) == 0){
										bestMove.x = x+1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									else if(board.getBoxAt(x+1, y) == enemyToken 
											&& board.getBoxAt(x-1, y) == 0){
										bestMove.x = x-1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
								}
								else if(x == 2){
									if(board.getBoxAt(x-1, y) == enemyToken
											&& board.getBoxAt(x-2, y) == 0){
										bestMove.x = x-2;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									else if(board.getBoxAt(x-2, y) == enemyToken 
											&& board.getBoxAt(x-1, y) == 0){
										bestMove.x = x-1;
										bestMove.y = y;
										moveFound = true;
										break;
									}
									//Corner Box. Look for diagonal chances too:
									else if(board.getBoxAt(1, 1) == enemyToken 
											&& board.getBoxAt(0, 0)==0){
										bestMove.x = 0;
										bestMove.y = 0;
										moveFound = true;
										break;
									}
								}
							}
						}
					}
				}				
			}
			if(!moveFound){
				//Fill in first encountered empty space:
				for (int x = 0; x < 3; x++) {
		            for (int y = 0; y < 3; y++) {
		                if(board.getBoxAt(x, y) == 0){
		                	bestMove.x = x;
							bestMove.y = y;
							moveFound = true;
							break;
		                }
		            }
		        }
			}
			return bestMove;
		}
	}
	
}
	