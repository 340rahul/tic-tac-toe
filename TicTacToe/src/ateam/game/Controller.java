package ATeam.game;

import java.awt.Point;

import ATeam.dat.Status;
import ATeam.mvc.Observer;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : THis is the Controller part in Model View Controller pattern/  
*               Implements Status type of Observer class
Name of ADT: View
Sets
*       Status  : Generic type that contains the set of functions that manages the status
*       Board   : Generic type that contains the set of functions that manages the board
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class Controller implements Observer<Status> {
	
	private Board board;
	
	/********************************************************************
	Function: Controller
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] Create -> Controller c
	- Precondition	: 
	- Postcondition : c is valid && the board is set as the variable b.
	********************************************************************/
	public Controller(Board b){
		
		this.board = b;
		
	}
	
	/********************************************************************
	Function: playermove
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] playermove(Controller c, Point point, char token) -> Controller c'
	- Precondition	: true
	- Postcondition : c' = c where player is moved to the specific point 
	********************************************************************/
	public void playermove(Point point, char token) {
		
        board.movePlayer(point, token);
        
    }
	
	/********************************************************************
	Function: cleanBoard
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] cleanBoard(Controller c) -> Controller c'
	- Precondition	: true
	- Postcondition : c' = c where board is cleaned
	********************************************************************/
	public void cleanBoard(){
		
		board.clean();
		
	}
	
	/********************************************************************
	Function: update
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] update(Controller c, Status status) -> Controller c'
	- Precondition	: true
	- Postcondition : c' = c where c is updated
	********************************************************************/
	public void update(Status status) {
	
	
	}
	
}
