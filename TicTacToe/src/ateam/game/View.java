package ATeam.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import ATeam.dat.Status;
import ATeam.mvc.Observer;
import ATeam.mvc.Viewable;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : View part in Model View Controller pattern/ 
*             Extends JFrame for UI, 
*             Implements viewable class(interface for view class),
*             MouseListener class(manage mouse events),
*             and Observer class
Name of ADT: View
Sets
* 	View: set of functions that manages the view
*       Int: Primitive integer type(� -1, 0, 1, 2 �)
*       ArrayList<JLabel>: Generic type that stores JLabel type elements
*       JPanel  : Generic type that controls the Panel
*       JLabel  : Generic type that controls the label
*       Board   : Generic type that contains sets of functions that manages the board
*       Controller: Generic type that contains sets of functions that manages controller
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
*       AI      : Generic type that manages the AI

Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/
public class View extends JFrame implements Viewable, MouseListener, Observer<Status> {

	private ArrayList <JLabel> boxes = new ArrayList <JLabel> ();
	private JPanel panel, messagePanel;
	private JLabel topText;
	private Board board;
	private Controller ctr;	
	private char player1;
	private char player2;
	private char currentPlayer;
	private AI bot1;
	
	/********************************************************************
	Function: View
	*********************************************************************
	DESCRIPTION :
	�	Constructor
	[1] Create -> View v
	- Precondition      : true
	- Postcondition     : v is valid &&
	*               Variables such as board, ctr, player1, player2, currentPlayer bot1
	*                   are initialized.
	********************************************************************/
	public View(Board b){
		
		this.board = b;
		ctr = new Controller(this.board);
		board.register(ctr);
		board.register(this);
		initUI();
		player1 = board.getToken(0);
		player2 = board.getToken(1);
		currentPlayer = player1;	//Player one starts the game.
		bot1 = new AI(this.board, this.player2, this.player1);
	}
	
	/********************************************************************
	Function: setupFrame
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setupFrame(View v) -> View v'
	- Precondition	: true
	- Postcondition: v' = v with the changed size of the frame to 400, 400,
	*                   the title of "TicTacToe",
	*                   the default close operation, EXIT_ON_CLOSE,
	*                   the location relative to null,
	*                   and layout as borderLayout.                 
	********************************************************************/
	private void setupFrame(){
		
		setSize(400, 400);
        setTitle("TicTacToe");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
		
	}
	
	/********************************************************************
	Function: getCurrentPlayer
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getCurrentPlayer(View v) -> char currentPlayer
	- Precondition	: true
	- Postcondition: returns the character value of currentPlayer.              
	********************************************************************/
	public char getCurrentPlayer(){
		
		return this.currentPlayer;
		
	}
	
	/********************************************************************
	Function: setCurrentPlayer
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setCurrentPlayer(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the modified variable of currentPlayer with the player.             
	********************************************************************/
	public void setCurrentPlayer(char player){
		
		this.currentPlayer = player;
		
	}
	
	/********************************************************************
	Function: setupMessagePanel
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setupMessagePanel(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the new JPanel,
	*                   new JLabel with the message "Player started as X",
	*                   the topText added to the mesagePanel 
	*                   and messagePanel added to the north of BorderLayout of contentPane.
	********************************************************************/
	private void setupMessagePanel() {
		
        messagePanel = new JPanel();
        topText = new JLabel("Player started as X");
        messagePanel.add(topText);

        this.getContentPane().add(messagePanel, BorderLayout.NORTH);
    }
	
	/********************************************************************
	Function: setupPanel
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setupPanel(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where panel is initialized with new JPanel type
	*               and layout is set as 3, 3, -1, -1
	********************************************************************/
	private void setupPanel() {
		
        panel = new JPanel();

        panel.setLayout(new GridLayout(3, 3, -1, -1));
        
    }
	
	/********************************************************************
	Function: setupBoard
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setupBoard(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the text is all initialized to null,
	*                   each dimension is set as 400/3 by 400/3,
	*                   Opoaque was set as true to allow coloring tothe background,
	*                   background is set as lightgray
	*                   horizontal alignment is set as center
	*                   and border is set as the thick of 2 and color CYAN
	********************************************************************/
	private void setupBoard() {
		
        for (int i = 0; i < 9; i++) {
        	
            JLabel symbol = new JLabel();
            symbol.setText("");
            symbol.setSize(new Dimension(400 / 3, 400 / 3));
            symbol.setOpaque(true);
            symbol.setBackground(Color.lightGray);
            symbol.setHorizontalAlignment(JLabel.CENTER);
            symbol.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.CYAN));
         

            boxes.add(symbol);
            panel.add(symbol);
            
        }
    }
	
	/********************************************************************
	Function: initUI
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setCurrentPlayer(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the frame, messagePanel, Panel and board is set
	********************************************************************/
	public void initUI() {
		
		setupFrame();
        setupMessagePanel();
        setupPanel();
        setupBoard();

        this.getContentPane().add(panel);
        panel.addMouseListener(this);
        	
	}
	
	/********************************************************************
	Function: drawSymbol
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] drawSymbol(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the background set as red for player O
	*                   and orange for player X
	********************************************************************/
	private void drawSymbol(int index, char symbol) {
		
        if (symbol == 'O') {
        	
            boxes.get(index).setBackground(Color.red);
            
        } 
        
        else {
        	
            boxes.get(index).setBackground(Color.ORANGE);
            
        }
    }
	
	/********************************************************************
	Function: mouseClicked
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mouseClicked(View v) -> View v'
	- Precondition	: true
	- Postcondition : v'= v with the event sent when the mouse is clicked            
	********************************************************************/
	public void mouseClicked(MouseEvent arg0) {
		
		
	}

	/********************************************************************
	Function: mouseEntered
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mouseEntered(View v) -> View v'
	- Precondition      : true
	- Postcondition     : v'= v where the event is thrown when the mouse is entered            
	********************************************************************/
	public void mouseEntered(MouseEvent arg0) {
		
		
	}

	/********************************************************************
	Function: mouseExited
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mouseExited(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the event is sent when the mouse is exited
	********************************************************************/
	public void mouseExited(MouseEvent arg0) {
		
		
	}

	/********************************************************************
	Function: mousePressed
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] mousePressed(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where the event is thrown when the mouse is pressed
	********************************************************************/
	public void mousePressed(MouseEvent arg0) {
		
		
	}

	/********************************************************************
	Function: mouseReleased
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setCurrentPlayer(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where event is thrown when the mouse is released.
	*                   When it is released, it passes the information to the controller with the 
	*                   playermove method
	********************************************************************/
	public void mouseReleased(MouseEvent arg0) {
		
		for (int i = 0; i < boxes.size(); i++) {

            //if one of the JLabels bounds contains the point where the mouse click 
            //was released, passes the information to the controller with the  
            //playermove method
            if (boxes.get(i).getBounds().contains(arg0.getPoint())) {
            	
            	ctr.playermove(new Point(i / 3, i % 3), currentPlayer);
                
            }
        }
		
	}
	
	/********************************************************************
	Function: clean
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] clean(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where every boxes of label is cleaned with null text
	*               and set every color as light gray(default color)
	********************************************************************/
	private void clean() {
		
        for (JLabel label : boxes) {
            label.setText("");
        }
        
        for(int index = 0; index < boxes.size(); index++){
        	
        	boxes.get(index).setBackground(Color.lightGray);
        	
        }

        panel.repaint();
     }
	
	/********************************************************************
	Function: switchPlayer
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] switchPlayer(View v) -> View v'
	- Precondition      : true
	- Postcondition     : v'= v where the current player is switched.
	*                       (if currentPlayer is player1, change it to player2
	*                       else, change it to player 1)
	********************************************************************/
	public void switchPlayer(){
		
		if(this.currentPlayer == this.player1){
			this.setCurrentPlayer(this.player2);
			
			//Let AI bot handle the move for player2:
			ctr.playermove(bot1.thinkMove(), currentPlayer);
		}
		else{
			this.setCurrentPlayer(this.player1);
		}
	}
	
	/********************************************************************
	Function: playerMove
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] playerMove(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v with the modified variable of currentPlayer with the player.             
	********************************************************************/
	private void playerMove(int position, char token){
		
		drawSymbol(position, token);
		
	}
	
	/********************************************************************
	Function: update
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] update(View v) -> View v'
	- Precondition      : true
	- Postcondition     : v'= v where the status of type is move, player move and switch the player
	*                   if the status type is win, set the toptext as showing who won,
	*                   if the status type is tie, set the totext as showing the game is a tie
	*                   and if the status type is cln,  than clean everything and start new AI.
	********************************************************************/
	public void update(Status status) {
		
		//If a MOV command is received from the board:
		if(status.getType() == "MOV"){
			this.playerMove(status.getPosition().y + 3 * status.getPosition().x, status.getToken());
			this.switchPlayer();
		}
		
		if(status.getType() == "WIN"){
			
			JOptionPane.showMessageDialog(this, status.getToken() + " Wins!", "Game Ended", JOptionPane.INFORMATION_MESSAGE);
			askPlayer();
		}
		
		if(status.getType() == "TIE"){
			String preText = topText.getText();
			JOptionPane.showMessageDialog(this, "The game is a Tie :-|", "Game Ended", JOptionPane.INFORMATION_MESSAGE);
			askPlayer();
		}
		
		if(status.getType() == "CLN"){
			this.clean();
			bot1 = new AI(this.board, player2, player1);
		}
		
	}
	
	/********************************************************************
	Function: askPlayer
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] askPlayer(View v) -> View v'
	- Precondition	: true
	- Postcondition: v'= v where if the answer from the player is yes, clean the board
	*                   and if the answer is no, than exit the window.
	********************************************************************/
	private void askPlayer(){
		
		Object [] options = {"Yes!", "Nope"};
		int response = JOptionPane.showOptionDialog(this, "Nice Play! Do you want to restart?",
				"Game Ended", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null
				, options, options[1]);
		
		if(response == 0){
			
			ctr.cleanBoard();
			
		}
		else{
			this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
		}
		
	}
		
}
