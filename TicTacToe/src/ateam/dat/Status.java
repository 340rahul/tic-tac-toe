package ATeam.dat;

import java.awt.Point;

/********************************************************************
***  NAME       :  Jamie Lee, Rahul Tripathi
***  ASSIGNMENT :  5
***  INSTRUCTOR :  Dr.Liu
*********************************************************************
DESCRIPTION : This class manages the status such as token and the position
Name of ADT: Status
Sets
*       Status  : set of funtions that manages the status
*       Point   : Java 2D point type
*       String  : Generic type to store strings
*       char    : Primitive character type( ..., 'a', 'A', 'b', 'B', ...)
*       Int		: Primitive integer type(� -1, 0, 1, 2 �)
*       boolean : Primitive boolean type

Interface Invariant: Once the ADT is created, it is always true that ADT is valid ADT.
********************************************************************/

public class Status {
	
	private String type;
	private char token;
	private Point position;
	
	/********************************************************************
	Function: Status
	*********************************************************************
	DESCRIPTION :
	�	Constructor
	[1] Create -> Status s
	- Precondition      : 
	- Postcondition     : s is valid && type, token and position is set
	********************************************************************/
	public Status(String type, char token, Point position){
		
		this.setType(type);
		this.setToken(token);
		this.setPosition(position);
				
	}

	/********************************************************************
	Function: getType
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getType(Status s) -> String type
	- Precondition	: true
	- Postcondition : returns the type 
	********************************************************************/
	public String getType() {
		
		return type;
		
	}
	
	/********************************************************************
	Function: setType
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setType(Status s, String type) -> Status s'
	- Precondition	: true
	- Postcondition : s' = s where type is set by the string 'type'
	********************************************************************/
	public void setType(String type) {
		
		this.type = type;
		
	}

	/********************************************************************
	Function: getToken
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getToken(Status s) -> char token
	- Precondition	: true
	- Postcondition : returns the 'token'
	********************************************************************/
	public char getToken() {
		
		return token;
		
	}

	/********************************************************************
	Function: setToken
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setToken(Status s, char token) -> Status s'
	- Precondition	: true
	- Postcondition : s' = s where token is set as the 'token'
	********************************************************************/
	public void setToken(char token) {
		
		this.token = token;
		
	}

	/********************************************************************
	Function: getPosition
	*********************************************************************
	DESCRIPTION :
	�	Accessor
	[1] getPosition(Status s) -> Point position
	- Precondition	: true
	- Postcondition : returns the 'position'
	********************************************************************/
	public Point getPosition() {
		
		return position;
		
	}
	
	/********************************************************************
	Function: setPosition
	*********************************************************************
	DESCRIPTION :
	�	Mutator
	[1] setPosition(Status s, Point position) -> Status s'
	- Precondition	: true
	- Postcondition : s' = s where position is set by the Point position.
	********************************************************************/
	public void setPosition(Point position) {
		
		this.position = position;
		
	}

}
